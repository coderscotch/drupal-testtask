<?php

namespace Drupal\weather\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Serialization\Json;
/**
 * Defines WeatherController class.
 */
class WeatherController extends ControllerBase {

  public function content($city_name, $country_code) {
    $config = $this->config('weather.settings');

    $url = $config->get('api_endpoint').'/?q=';
    $url .= (!empty($city_name)) ? $city_name : $config->get('city_name');
    if(!empty($city_name)) {
      $url .= (!empty($country_code)) ? ','.$country_code : '';
    } else {
      $url .= (!empty($config->get('country_code'))) ? ','.$config->get('country_code') : '';
    }
    $url .= '&APPID='.$config->get('api_key');
    
    $data = [];
    $client = \Drupal::httpClient();
    try {
      $request = $client->request('GET', $url);
      $status = $request->getStatusCode();
      $data = JSON::decode($request->getBody()->getContents());
    }
    catch (\Exception $e) {
      if ($e->hasResponse()) {
        $data = JSON::decode($e->getResponse()->getBody()->getContents());
      }
    }
    
    if($data['cod'] == 200) {
      $headers = [$this->t('City, Country Code'), $this->t('Weather'), $this->t('Wind'), $this->t('Others')];

      $weather_info['data'] = [];
      if(!empty($data['weather'])) {
        foreach ($data['weather'] as $weather) {
          $weather_info['data'][] = [
            '#type' => 'inline_template',
            '#template' => '<div><strong>{{ main }}:</strong> {{ description }}</div>',
            '#context' => [
              'main' => $this->t($weather['main']),
              'description' => $this->t($weather['description'])
            ]
          ];
        }
      }

      $wind_info['data'] = [];
      if(!empty($data['wind'])) {
        foreach ($data['wind'] as $key => $value) {
          $wind_info['data'][] = [
            '#type' => 'inline_template',
            '#template' => '<div><strong>{{ title }}:</strong> {{ value }}</div>',
            '#context' => [
              'title' => $this->t(ucfirst(str_replace('_', ' ', $key))),
              'value' => $value
            ]
          ];
        }
      }

      $other_info['data'] = [];
      if(!empty($data['main'])) {
        foreach ($data['main'] as $key => $value) {
          $other_info['data'][] = [
            '#type' => 'inline_template',
            '#template' => '<div><strong>{{ title }}:</strong> {{ value }}</div>',
            '#context' => [
              'title' => $this->t(ucfirst(str_replace('_', ' ', $key))),
              'value' => $value
            ]
          ];
        }
      }

      $rows[] = [
        $data['name'].', '.$data['sys']['country'],
        $weather_info,
        $wind_info,
        $other_info
      ];

      $build['tinfo'] = [
        '#type' => 'table',
        '#caption' => $this->t('Current Weather Info'),
        '#header' => $headers,
        '#rows' => $rows,
        '#empty' => $this->t('City not found.'),
      ];
    } else {
      $build['info'] = [
        '#type' => 'inline_template',
        '#template' => '<div class="content">{{ message }}</div>',
        '#context' => [
          'message' => ($data['message']) ?? $this->t('Something went worng!!')
        ]
      ];
    } 
    return $build;
  }

}
