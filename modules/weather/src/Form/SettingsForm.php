<?php
namespace Drupal\weather\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {

	public function getFormId() {
    	return 'weather_settings_admin_form';
  	}

  	protected function getEditableConfigNames() {
	    return ['weather.settings'];
	}

	public function buildForm(array $form, FormStateInterface $form_state) {
		$config = $this->config('weather.settings');
		$form['city_name'] = [
			'#type' => 'textfield',
			'#title' => $this->t('City Name'),
			'#default_value' => $config->get('city_name'),
			'#description' => t("Set city name for which the current weather is displaying."),
			'#required' => TRUE,
	    ];
	    $form['country_code'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Country Code'),
			'#default_value' => $config->get('country_code'),
			'#description' => t("Optionally set coutry code for which weather page displays the current weather.")
	    ];
	    $form['api_endpoint'] = [
			'#type' => 'textfield',
			'#title' => $this->t('API End Point URL'),
			'#default_value' => $config->get('api_endpoint'),
			'#description' => t("Set API from which you want to get the weather information."),
			'#required' => TRUE,
	    ];
	    $form['api_key'] = [
			'#type' => 'textfield',
			'#title' => $this->t('API Key'),
			'#default_value' => $config->get('api_key'),
			'#description' => t("Set API Key for your provided API endpoint."),
			'#required' => TRUE,
	    ];

	    return parent::buildForm($form, $form_state);
	}

	public function submitForm(array &$form, FormStateInterface $form_state) {
		$this->config('weather.settings')
	      ->set('city_name', $form_state->getValue('city_name'))
	      ->set('country_code', $form_state->getValue('country_code'))
	      ->set('api_endpoint', $form_state->getValue('api_endpoint'))
	      ->set('api_key', $form_state->getValue('api_key'))
	      ->save();

    	parent::submitForm($form, $form_state);
	}
}
?>